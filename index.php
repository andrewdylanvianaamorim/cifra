<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src='js/index.js' defer type='module'></script>
</head>
<body>
    <p>Escreva sua menssagem aqui: </p>
    <textarea name="" id="msg" cols="30" rows="10"></textarea>
    <p>Aqui ela será descriptografada:</p>
    <textarea name="" id="msg_cripto" cols="30" rows="10"></textarea>
    <!--Atenção Jonathan: Fazer o resto do alfabeto :)-->
    <select id='letra'>
        <option value='a'>a</option>
        <option value='z'>z</option>
        <option value='x'>x</option>
        <option value='d'>d</option>
    </select>
</body>
</html>