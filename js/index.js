'use strict'
import {gerar_alfabeto, gerar_chave, criptografar, descriptografar} from '../libs/cifra/cifra.js'

let msg = document.getElementById('msg');

let msg_cripto = document.getElementById('msg_cripto');

let letra = document.getElementById('letra');

letra.onchange = () => {
    msg_cripto.value = criptografar(letra.value, msg.value);
}

msg.oninput = () => {
    msg_cripto.value = criptografar(letra.value, msg.value);
}
