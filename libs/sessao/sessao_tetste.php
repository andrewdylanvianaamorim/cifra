<?php

include_once('sessao.php');


$sessao = cria_ou_restaura_sessao();

if (!file_exists(TMP . SEPARADOR . 'sessao.txt')) {
    echo '<br>Erro: na primeira chamada de preparar_ambiente()';
    echo '<br>Erro: Falha ao cria o arquivo de sessão!';
}

if (!file_exists(TMP . SEPARADOR . 'trava.txt')) {
    echo '<br>Erro: na primeira chamada de preparar_ambiente()';
    echo '<br>Erro: Falha ao cria o arquivo de trava!';
}

$stats_antigos =  $sessao->pegar_stats();

$sessao = cria_ou_restaura_sessao();

$stats_novos = $sessao->pegar_stats();

if ($stats_novos[0]['ctime'] - $stats_antigos[0]['ctime'] != 0) {
    echo '<br>Erro: criar_ou_restaurar_sessao() está mudando de chamada a chamada';
    echo '<br>Erro: arquivo de sessão foi refeito';
}

if ($stats_novos[1]['ctime'] - $stats_antigos[1]['ctime'] != 0) {
    echo '<br>Erro: criar_ou_restaurar_sessao() está mudando de chamada a chamada';
    echo '<br>Erro: arquivo de trava foi refeito';
}


