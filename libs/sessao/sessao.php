<?php
/*
    Neste arquivo vai tudo relacionado a sessão

    NOTA: NÃO USE AS FUNÇÔES DE LEITURA E ESCRITA DE ARQUIVOS NO sessao.txt
          E lembre-se de configurar o arquivo config_session.php para se adequar
          ao seu sistema

    NOTA: O primeir jogador vai ser o que vai criptografar
*/

include_once('../cifra/cifra.php');
include_once('config_session.php');

// //Essa constante vai estar no arquivo, de trava, quando ele estiver travado
// define('CARACTERE_DE_TRAVA', 'T');
// //Essa constante vai estar no arquivo, de trava, quando ele estiver liberado
// define('CARACTERE_DE_LIBERACAO', 'L');


define('TAMANHO_ID', 6);

//Esses são os estados de jogos

enum EstadoDeJogo {
    case esperando_outro_jogador;
    case esperando_menssagem;
    case esperando_primeiro_jogador;
    case jogando;
    case terminado;
}

class Sessao 
{
    private $id;
    private $primeira_letra;
    private $msg_criptografada;
    private $estado_de_jogo;
    private $tempo_de_criacao;
    private $primeiro_jogador;
    private $arquivo_para_salvar;

    
    public function __constructor($arquivo_para_salvar, $objeto_vazio = false)
    {
        //constroium objeto vazio
        if (!$objeto_vazio) return;

        $this->arquivo_para_salvar = $arquivo_para_salvar;
        session_start();
        //verificando se já há uma sessão existente
        if (array_key_exists('SESSAO', $_SESSION)) {
            $sessao_antiga = $_SESSION['SESSAO'];
            $this->id = $sessao_antiga.get_id();
            $this->primeira_letra = $sessao_antiga.get_primeira_letra();
            $this->msg_criptografada = $sessao_antiga.get_msg_criptografada();
            $this->estado_de_jogo = $sessao_antiga.get_estado_de_jogo();
            $this->primeiro_jogador = $sessao_antiga.get_primeiro_jogador();
            $this->tempo_de_criacao = $sessao_antiga.get_tempo_de_criacao();
        } else {
            //verifica se tem algum jogo esperando se tiver
            //vamos entrar nele, copiando assim sua sessão e mudando o estado de jogo
            //caso não exista vamos criar uma sessão do zero
            $jogo_para_se_ajuntar = Sessao::pega_jogo_para_se_juntar();
            if ($jogo_para_se_ajuntar === false) {
                $this->id = $this->gerar_id();
                $this->primeira_letra = null;
                $this->msg_criptografada = null;
                $this->estado_de_jogo = EstadoDeJogo::esperando_outro_jogador;
                $this->primeiro_jogador = true;
                $_SESSION['SESSAO'] = $this;
                return;
            } else {
                $this->id = $jogo_para_se_ajuntar.get_id();
                $this->primeira_letra = $jogo_para_se_ajuntar.get_primeira_letra();
                $this->msg_criptografada = $jogo_para_se_ajuntar.get_msg_criptografada();
                $this->estado_de_jogo = $jogo_para_se_ajuntar.get_estado_de_jogo();
                $this->primeiro_jogador = $jogo_para_se_ajuntar.get_primeiro_jogador();
                $this->tempo_de_criacao = $jogo_para_se_ajuntar.get_tempo_de_criacao();
            }
        }
    }

    //retorna a sessão do jogo em para se juntar ou
    //false caso não tenha nenhum jogo 
    private static function pega_jogo_para_se_juntar() {
        $sessoes_abertas = Sessao::pegar_sessoes_abertas();
        foreach ($sessoes_abertas as  $s) {
            if ($s.get_estado_de_jogo() === EstadoDeJogo::esperando_outro_jogador) return s;
        }
        return false;
    }


    //retorna um array com as sessoes que estão abertas
    private static function pegar_sessoes_abertas() {
        $sessoes_abertas = [];
        $todas_as_sessoes = Sessao::pegar_todas_as_sessoes();
        foreach ($todas_as_sessoes as $s) {
            if (s.get_estado_de_jogo() != EstadoDeJogo::terminado) {
                array_push($sessoes_abertas, s);
            }
        }
        return $sessoes_abertas;
    }


    public function get_id(){
        return $this->id;
    }

    public function get_primeira_letra(){
        return $this->primeira_letra;
    }

    public function get_msg_criptografada() {
        return $this->msg_criptografada;
    }

    public function get_estado_de_jogo() {
        return $this->estado_de_jogo;
    }

    public function get_tempo_de_criacao() {
        return $this->tempo_de_criacao;
    }

    public function get_primeiro_jogador() {
        return $this->primeiro_jogador;
    }
    

    private function pegar_ids_antigos() {
        fseek($this->arquivo_para_salvar, 0);
        $tamanho = fstat($this->arquivo_para_salvar)['size'];
        if ( $tamanho == 0) {
            return [];
        } else {
            $conteudo_do_arquivo = fread($this->arquivo_para_salvar,$tamanho);
            if (!str_contains($conteudo_do_arquivo, '<id>')) {
                return [];
            }

            //onde devemos começar a procura na string
            $offset = 0;
            $ids_antigos = [];
            while (true) {
                //começamos procurando por <id>
                $comeco = strpos($conteudo_do_arquivo, '<id>', $offset);
                //se o começo for falso chegamos ao final da busca
                if ($comeco === false) {
                    break;
                }

                $fim = strpos($conteudo_do_arquivo, '</id>', $offset);

                //setamos o offset para a o final dessa busca para podermos continuar procurando
                $offset = $fim + strlen('</id>');

                //pegando o id e colocando ele no resultado
                array_push(
                    $ids_antigos, 
                    substr($conteudo_do_arquivo, $comeco + strlen('<id>'),TAMANHO_ID)
                );
            }
            return $ids_antigos;
        }
    }

    private function gerar_id() {
        $ids_ja_usados = $this->pegar_ids_antigos();
        $id_gerado_Ja_esta_sendo_usado = false;
        $id_gerado = '';
        $caracteres_do_id = gerar_alfabeto() + '0123456789';

        do {
            $id_gerado = '';
            for ($i=0; $i < TAMANHO_ID; $i++) { 
                $id_gerado += $caracteres_do_id[random_int(0,count($ids_ja_usados) - 1)];
            }
            $id_gerado_Ja_esta_sendo_usado = array_search($id_gerado, $ids_ja_usados) === false ? false : true;
        } while($id_gerado_Ja_esta_sendo_usado);

        return $id_gerado;
    }
}