<?php
/*
    Aqui ficaram as configurações do sistema
    recomendo que se faça um backup desse arquivo
    antes de modifica-lo

    obs: Por padrão este arquivo está configurado para rodar no LINUX
*/

/* 
    Essa constante deve conter o caminho para a pasta de arquivos temporario do sistema
    em que o servidor esteja rodando.

    Ex: 
        Linux: /tmp
        windows: o resultado de %TEMP%
*/
define('TMP', '/tmp');


/* Essa constante define o tempo máximo das sessão em SEGUNDOS*/
define('TEMPO_MAXIMO', 60);

/* 
    Essa constante define o separador de caminho

    Ex:
        Linux: /
        Window: \\
*/
define('SEPARADOR', '/');
