<?php

// Essa função ira percorer a $msg_para_criptografar
// e ver se o caractere é do alfabeto se sim vamos
// pegar a posição do caractere no alfabeto e substituir
// pela letra na mesma posição da chave(alfabeto cifrado).
// Caso o caractere não se encontar vamos simplesmente colocar
// ele do jeito que está(agradeça ao ótimo suporte do php a utf-8)
// OBS: talvez tenhamos problemas uma vez que js usa utf-16 mas vamos ver
function criptografar($primeira_letra, $msg_para_criptografar) {
    $msg_criptografada = '';
    $msg_para_criptografar = strtolower($msg_para_criptografar);
    $chave = gerar_chave($primeira_letra);
    $alfabeto = gerar_alfabeto();

    for ($i = 0; $i < strlen($msg_para_criptografar); $i++) {
        $caractere = substr($msg_para_criptografar, $i, 1);
        // verificando se o caractere esta no alfabeto
        // no caso faremos isso usando o retorno de strpos
        // que retorna falso caso não esteja e se estiver
        // vai dar a posiçao que nos já precisamos
        
        $posicao_no_alfabeto = strpos($alfabeto, $caractere, 0);
        if ($posicao_no_alfabeto == false) {

            // isso é um hack para um comportamento que eu não sei o porque
            // estava acontescendo. Simplesmente strpos(strpos($alfabeto, 'a', 0)) retonava falso
            if($caractere == 'a') {
                $msg_criptografada .= substr($chave, 0, 1);
                continue;
            }

            $msg_criptografada .= $caractere;
        } else {
            // agora nos pegamos a posição e usamos a equivalente
            // na chave(alfabeto cifrado)
            $msg_criptografada .= substr($chave, $posicao_no_alfabeto, 1);
           
        }
    }
    return $msg_criptografada;
}

// Nessa função nos geramos a chave e iteramos pelos caracteres
// de $msg_criptografada e vamos então checaremos se o caractere
// esta na chave, se está vamos pegar a posição dele , na chave, e adicionar
// na string descriptografada o caractere na mesma posição no alfabeto normal.
// Em resumo: O inverso do que fizemos na criptografia
function descriptografar($primeira_letra, $msg_criptografada) {
    $chave = gerar_chave($primeira_letra);
    $alfabeto = gerar_alfabeto();
    $msg_criptografada = strtolower($msg_criptografada);
    $msg_descriptografada = '';

    for ($i=0; $i < strlen($msg_criptografada); $i++) { 
        $caractere = substr($msg_criptografada, $i, 1);

        $posicao_na_chave = strpos($chave, $caractere);

        if ($posicao_na_chave == false) {
            if ($caractere == $primeira_letra) {
                $msg_descriptografada .= substr($alfabeto,0,1);
                continue;
            }
            $msg_descriptografada .= $caractere;
        } else {
            $msg_descriptografada .= substr($alfabeto, $posicao_na_chave, 1);
        }
    }

    return $msg_descriptografada;
}

// esta função vai gerar o alfabeto criptografado
//o algorítimo vai ser o seguinte começaremos com
//a primeira letra e vamos gerando as proximas com
//base no seu código ascii e quando chegarmos em 'z'
//nos vamos voltar o alfabeto para onde começou.
//Exceto nos casos da primeira letra ser a ou z
function gerar_chave($primeira_letra) {
    $chave = '';
    if ($primeira_letra == 'a') {
        $chave = gerar_alfabeto();
    } else if ($primeira_letra == 'z') {
        //printf("%d %d<br>", ord('a'), ord('z'));
        for ($i=ord('z'); $i >= ord('a'); $i--) {
            
            $chave .= chr($i);
        }
    } else {
        //indo da primeira_letra até z
        for ($i = ord($primeira_letra); $i <= ord('z'); $i++) {
            $chave .= chr($i);
        }

        //indo de a até a primeira letra, sem incluir ela é claro
        for ($i = ord('a'); $i < ord($primeira_letra); $i++) {
            $chave .= chr($i);
        }
    }

    return $chave;
}

function gerar_alfabeto() {
    $alfabeto = '';
    for ($i = ord('a'); $i <= ord('z'); $i++) {
        $alfabeto .= chr($i);
    }
    return $alfabeto;
}