'use strict';

// esse arquivo é simplesmente a transformação do cifra.php
// para jasvascript então a documentação de lá se aplica 


export function gerar_alfabeto() {
    let alfabeto = '';
    for (let i = 'a'.charCodeAt(0); i <= 'z'.charCodeAt(0); i++)
        alfabeto += String.fromCharCode(i);
    return alfabeto;
}

export function gerar_chave(primeira_letra) {
    let chave = '';
    if (primeira_letra === 'a') {
        chave = gerar_alfabeto();
    } else if (primeira_letra === 'z'){
        for (let i = 'z'.charCodeAt(0); i >= 'a'.charCodeAt(0); i--) {
            chave += String.fromCharCode(i);
        }
    } else {
        for (let i = primeira_letra.charCodeAt(0); i <= 'z'.charCodeAt(0); i++) {
            chave += String.fromCharCode(i);
        }

        for (let i = 'a'.charCodeAt('a'); i < primeira_letra.charCodeAt(0); i++) {
            chave += String.fromCharCode(i);
        }
    }

    return chave;
}

export function criptografar(primeira_letra, msg_para_criptografar) {
    let msg_criptografada = '';
    msg_para_criptografar = msg_para_criptografar.toLowerCase();
    let chave = gerar_chave(primeira_letra);
    let alfabeto = gerar_alfabeto();

    for (let index = 0; index < msg_para_criptografar.length; index++) {
        const caractere = msg_para_criptografar[index];
        
        let posicao_no_alfabeto = alfabeto.indexOf(caractere);
        
        //caso o caractere não esteja no alfabeto
        if (posicao_no_alfabeto == -1) {
            msg_criptografada += caractere;
        } else {
            msg_criptografada += chave[posicao_no_alfabeto];
        }
    }

    return msg_criptografada;
}

export function descriptografar(primeira_letra, msg_criptografada) {
    let chave = gerar_chave(primeira_letra);
    let alfabeto = gerar_alfabeto();
    msg_criptografada = msg_criptografada.toLowerCase();
    let msg_descriptografada = '';
    
    for (let index = 0; index < msg_criptografada.length; index++) {
        const caractere = msg_criptografada[index];

        let posicao_na_chave = chave.indexOf(caractere);

        //caso o caractere não esteja na chave
        if (posicao_na_chave == -1) {
            msg_criptografada += caractere;
        } else {
            msg_criptografada += alfabeto[posicao_na_chave];
        }
        
    }

    return msg_descriptografada;
}