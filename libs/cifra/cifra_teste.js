import {gerar_alfabeto, gerar_chave, criptografar, descriptografar} from './cifra.js'

console.log('se aparecer alguma coisa nessa página temos um erro');

if (gerar_alfabeto() != 'abcdefghijklmnopqrstuvwxyz') {
  console.log("erro:gerar_alfabeto()");
}

if (gerar_chave('a') != 'abcdefghijklmnopqrstuvwxyz') {
  console.log("erro: gerar_chave('a')");
}

if (gerar_chave('z') != 'zyxwvutsrqponmlkjihgfedcba') {
  console.log("erro: gerar_chave('z')");
}


if (gerar_chave('d') != 'defghijklmnopqrstuvwxyzabc') {
  console.log("erro: gerar_chave('d')");
}

if (criptografar('d', 'a ligeira raposa marrom saltou sobre o cachorro cansado') != 'd oljhlud udsrvd pduurp vdowrx vreuh r fdfkruur fdqvdgr'){
  console.log("erro: criptografar('d', 'a ligeira raposa marrom saltou sobre o cachorro cansado')");
}


if (criptografar('x', 'casa') != 'zxpx'){
  console.log("erro: criptografar('x', 'casa')");
  console.log(`resultado errado: ${criptografar('x', 'casa')}`);
}

if (criptografar('x', 'viu') != 'sfr'){
  console.log("erro: criptografar('x', 'viu')");
}

if (criptografar('h', 'ruim') != 'ybpt') {
  console.log("erro: criptografar('h', 'ruim') != 'ybpt')");
}


if (criptografar('h', 'abacaxi') != 'hihjhep') {
  console.log("erro: criptografar('h', 'abacaxi') != 'hihjhep')");
}

if (criptografar('a', 'abc') != 'abc') {
  console.log(" erro criptografar('a', 'abc')");
}

if (descriptografar('h', 'ybpt')  != 'ruim'){
  console.log("erro: descriptografar('h', 'ybpt')");
}



if (descriptografar('h', 'hb')  != 'au'){
  console.log("erro: descriptografar('h', 'hb')");
}


if (descriptografar('d', criptografar('d', 'a ligeira raposa marrom saltou sobre o cachorro cansado')) != 'a ligeira raposa marrom saltou sobre o cachorro cansado') {
  console.log("erro: descriptografar('d', criptografar('d', 'a ligeira raposa marrom saltou sobre o cachorro cansado'))");
}


